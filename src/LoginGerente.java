import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LoginGerente extends JFrame {
	private JLabel nome;
	private JLabel senha;
	private JTextField nomeG;
	private JPasswordField senhaG;
	private JButton ok;
	private JButton cancel;
	static TelaGerente telaGerente;
	
	public LoginGerente(){
		super("Login Gerente");
		setLayout(new GridLayout(3,2,0,20));
		nome = new JLabel("Nome:");
		nome.setHorizontalAlignment(SwingConstants.LEFT);
		add(nome);
		
		nomeG = new JTextField();
		add(nomeG);
		
		senha = new JLabel("Senha:");
		senha.setHorizontalAlignment(SwingConstants.LEFT);
		add(senha);
		
		senhaG = new JPasswordField();
		add(senhaG);
		
		ok = new JButton("Ok");
		add(ok);
		
		cancel = new JButton("Cancelar");
		add(cancel);
		
		telaGerente  = new TelaGerente();
		telaGerente.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		telaGerente.setLocation(400,20);
		telaGerente.setSize(800,800);
		
		
		LoginHandler handlerLogin = new LoginHandler();
		cancel.addActionListener(handlerLogin);
		ok.addActionListener(handlerLogin);
	}
	private class LoginHandler implements ActionListener{
		public void actionPerformed(ActionEvent event) {
			if(event.getSource() == cancel)
			{
				Banco.telaPrincipal.setVisible(true);
				TelaPrincipal.loginGerente.setVisible(false);
			}
			else if(event.getSource() == ok)
			{
				String nome = nomeG.getText();
				String senha = new String( senhaG.getPassword());
				if(Programa.banco.loginGerente(nome, senha))
				{
					telaGerente.setVisible(true);
					TelaPrincipal.loginGerente.setVisible(false);
				}
				else
				{
					JOptionPane.showMessageDialog(null,"Usu�rio ou senha Inv�lido","Login Gerente",JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}
}
