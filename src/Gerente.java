
public class Gerente extends Pessoa{
	private Cliente clientes[] = new Cliente[30];

	public Gerente(String nome, String senha) {
		super(nome, senha);
	}
	
	public boolean cadastraCliente(String nome, String senha)
	{
		int i;
		for(i=0; i<30; i++)
		{
			if(clientes[i] == null)
			{
				clientes[i] = new Cliente(nome,senha);
				return true;
			}
		}
		return false; //caso array de clientes ja esteja completo
	}

	public Cliente[] getClientes() {
		return clientes;
	}
	
	public Cliente getCliente(String nome) {
		for(int i=0; i<30 && clientes[i] != null; i++)//busca cliente na array
		{
			if((clientes[i].getNome()).equals(nome))
			{
				return clientes[i];// se cliente existe, retorna o cliente
			}
		}
		return null;//retorna cliente inexistente
	}
	
	public int aplica��o(String nome, int numero, double valor)
	{
		Cliente cliente;
		Conta contas[];
		
		cliente = getCliente(nome);//retorna cliente
		if(cliente!= null)
		{
			if(cliente.verificaConta(numero))
			{
				contas = cliente.getContas();
				contas[numero].depositar(valor);
				return 1;// exito
			}
			else
			{
				return -1;//conta nao existe
			}
		}
		return -2;// cliente nao existe
	}
	
	public int transferencia(String nomeOri, String nomeDes, int numeroOri, int numeroDes, double valor)
	{
		Cliente clienteOri, clienteDes;
		Conta contasOri[], contasDes[];
	
		clienteOri = getCliente(nomeOri);//retorna o cliente
		clienteDes = getCliente(nomeDes);
		if(clienteOri != null && clienteDes != null)//caso cliente nao exista, returna -2
		{
			if((clienteOri.verificaConta(numeroOri)) && (clienteDes.verificaConta(numeroDes)))
			{
				contasOri = clienteOri.getContas();
				contasDes = clienteDes.getContas();
				if(contasOri[numeroOri].sacar(valor))
				{
					contasDes[numeroDes].depositar(valor);
					return 1;//retorna exito
				}
				else
				{
					return 2;//retorna Saldo insuficiente
				}
			}
			else
			{
				return -1;//uma das contas nao existe
			}
		}
		return -2;// um dos clientes nao existe
	}
	
	public String getTipo(String nome, int numero)
	{
		Cliente cliente;
		Conta contas[];
		cliente = getCliente(nome);
		if(cliente != null)
		{
			contas = cliente.getContas();
			if(cliente.verificaConta(numero))
			{
				return contas[numero].getTipo();
			}
			else
			{
				return "c";//conta nao existe
			}
		}
		return null;//cliente nao existe
	}
	
	public int setLimite(String nome, int numero, double valor)
	{
		Cliente cliente;
		Conta contas[];
		String tipo;
		
		tipo = getTipo(nome,numero);
		if(tipo.equals("Especial"))
		{
			cliente = getCliente(nome);
			contas = cliente.getContas();
			((ContaEspecial)contas[numero]).setLimite(valor);
			return 1;
		}
		else if(tipo.equals("c"))
		{
			return -1; //conta nao existe
		}
		else if(tipo.equals(null))
		{
			return -2;//cliente nao exite
		}
		return -4;//conta diferente de especial
	}
	
	public double getLimite(String nome, int numero)
	{
		String tipo;
		Cliente cliente;
		Conta contas[];
		tipo = getTipo(nome,numero);
		if(tipo.equals("Especial"))
		{
			cliente = getCliente(nome);
			contas = cliente.getContas();
			return ((ContaEspecial)contas[numero]).getLimite();
		}
		else if(tipo.equals("c"))
		{
			return -1;//conta nao existe
		}
		else if(tipo.equals(null))
		{
			return -2; //cliente nao existe
		}
		return -4;//conta diferente da especial
	}
	
	public double setJuros(String nome, int numero, double valor)
	{
		Cliente cliente;
		String tipo;
		Conta contas[];
		tipo = getTipo(nome,numero);
		if(tipo.equals("Poupan�a"))
		{
			cliente = getCliente(nome);
			contas = cliente.getContas();
			((ContaPoupan�a)contas[numero]).setJuros(valor);
			return 1;
		}
		else if(tipo.equals("c"))
		{
			return -1; //conta nao existe
		}
		else if(tipo.equals(null))
		{
			return -2;//cliente nao exite
		}
		return -4;//conta diferente de poupan�a
	}
	
	public double getJuros(String nome, int numero)
	{
		String tipo;
		Cliente cliente;
		Conta contas[];
		
		tipo = getTipo(nome,numero);
		if(tipo.equals("Poupan�a"))
		{
			cliente = getCliente(nome);
			contas = cliente.getContas();
			return ((ContaPoupan�a)contas[numero]).getJuros();
		}
		else if(tipo.equals("c"))
		{
			return -1;//conta nao existe
		}
		else if(tipo.equals(null))
		{
			return -2; //cliente nao existe
		}
		return -4;//conta diferente da poupan�a
	}
	
	public int sacar(String nome, int numero, double valor)
	{
		if(valor>110000)
		{
			Cliente cliente;
			Conta contas[];
			
			cliente = getCliente(nome);
			if(cliente != null)
			{
				if(cliente.verificaConta(numero))
				{
					contas=cliente.getContas();
					if(contas[numero].sacar(valor))
					{
						return 1;
					}
					else
					{
						return 2;//saldo insuficiente
					}
				}
				else
				{
					return -1;//conta nao existe
				}
			}
			else
			{
				return -2;//cliente nao existe
			}
			
		}
		return 3; //valor menor que 110000
	}

}
