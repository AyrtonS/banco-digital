import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LoginCliente extends JFrame{
	private JLabel nomeL;
	private JLabel senha;
	private JTextField nome;
	private JPasswordField senhaC;
	private JButton ok;
	private JButton cancel;
	static TelaCliente telaCliente;
	
	public LoginCliente() {
		super("Login Cliente");
		setLayout(new GridLayout(3,2,0,20));
		nomeL = new JLabel("Nome:");
		nomeL.setHorizontalAlignment(SwingConstants.LEFT);
		add(nomeL);
		
		nome = new JTextField();
		add(nome);
		
		senha = new JLabel("Senha:");
		senha.setHorizontalAlignment(SwingConstants.LEFT);
		add(senha);
		
		senhaC = new JPasswordField();
		add(senhaC);
		
		ok = new JButton("Ok");
		add(ok);
		
		cancel = new JButton("Cancelar");
		add(cancel);
		telaCliente  = new TelaCliente();
		telaCliente.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		telaCliente.setLocation(400,20);
		telaCliente.setSize(800,800);
		
		LoginHandler handlerLogin = new LoginHandler();
		cancel.addActionListener(handlerLogin);
		ok.addActionListener(handlerLogin);
	}
	
	private class LoginHandler implements ActionListener{
		public void actionPerformed(ActionEvent event) {
			if(event.getSource() == cancel)
			{
				Banco.telaPrincipal.setVisible(true);
				TelaPrincipal.loginCliente.setVisible(false);
			}
			else if(event.getSource() == ok)
			{
				String nomeC = nome.getText();
				String senha = new String( senhaC.getPassword());
				if(Programa.banco.loginCliente(nomeC, senha))
				{
					telaCliente.setVisible(true);
					TelaPrincipal.loginCliente.setVisible(false);
				}
				else
				{
					JOptionPane.showMessageDialog(null,"Usu�rio ou senha Inv�lido","Login Cliente",JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}
}
