
public class ContaEspecial extends Conta 
{
	private double limite;
	
	public ContaEspecial(String tipo, String correntista, int numero) {
		super(tipo,correntista,numero);
	}
	
	public double getLimite()
	{
		return limite;
	}
	
	public void setLimite(double limite)
	{
		this.limite = limite;
	}
	
	public boolean sacar(double valor)
	{
		if((saldo + limite) >= valor )
		{
			saldo = saldo-valor;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	

}
