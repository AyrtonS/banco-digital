import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
public class TelaPrincipal extends JFrame{
		private JLabel tela1;
		private JButton gerente;
		private JButton cliente;
		static LoginCliente loginCliente;
		static LoginGerente loginGerente;
		public TelaPrincipal(){
			super("Banco Digital");
			setLayout(new GridLayout(3,0));
			tela1 = new JLabel("Selecione uma Op��o:");
			tela1.setHorizontalAlignment(SwingConstants.CENTER);
			add(tela1);
			
			gerente = new JButton("Gerente");
			add(gerente);
			
			cliente = new JButton("Cliente");
			
			add(cliente);
			
			loginCliente = new LoginCliente();
			loginCliente.setSize(400,200);
			loginCliente.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			loginCliente.setLocation(500,200);
			
			loginGerente = new LoginGerente();
			loginGerente.setSize(400,200);
			loginGerente.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			loginGerente.setLocation(500,200);
			
			ButtonHandler handler = new ButtonHandler();
			cliente.addActionListener(handler);
			gerente.addActionListener(handler);
		}
		
		private class ButtonHandler implements ActionListener{
			public void actionPerformed(ActionEvent event)
			{
				if(event.getSource()==gerente)
				{
					loginGerente.setVisible(true);
					Banco.telaPrincipal.setVisible(false);
				}
				if(event.getSource()==cliente)
				{
			    
					loginCliente.setVisible(true);
					Banco.telaPrincipal.setVisible(false);
				}
				
			}
		}
}
