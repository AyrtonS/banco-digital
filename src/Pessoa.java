
abstract class Pessoa {
	private String nome;
	private String senha;
	
	public Pessoa(String nome, String senha) {
		this.nome = nome;
		this.senha = senha;
	}
	
	public String getNome() {
		return nome;
	}
	
	public boolean verificaSenha(String senha) {
		if(this.senha.equals(senha))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean alteraSenha(String senhaAtual, String novaSenha) {
		if(senhaAtual.equals(senha))
		{
			senha = novaSenha;
			return true;
		}
		else
		{
			return false;
		}
	}

}
