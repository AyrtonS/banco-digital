import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
public class TelaGerente extends JFrame{
	
	private JButton retirada;
	private JButton aplicacao;
	private JButton transferencia;
	private JButton cadastrarCliente;
	private JButton	cadastrarGerente;
	private JButton alterarSenha;
	static CadastroGerente cadastroGerente;
	static CadastraCliente cadastraCliente;
	public TelaGerente(){
		super("Banco Digital");
		setLayout(new GridLayout(3,3,100,100));
		
		retirada = new JButton("Retirada");
		add(retirada);
		
		aplicacao = new JButton("Aplicacao");
		add(aplicacao);
		
		transferencia= new JButton("Transferencia");
		add(transferencia);
		
		cadastrarCliente = new JButton("Cadastrar Cliente");
		add(cadastrarCliente);
		
		cadastrarGerente =new JButton("Cadastrar Gerente");
		add(cadastrarGerente);
		
		alterarSenha = new JButton("Aterar Senha");
		add(alterarSenha);
		
		GerenteHandler handlerGerente = new GerenteHandler();
		cadastrarGerente.addActionListener(handlerGerente);
		cadastrarCliente.addActionListener(handlerGerente);
	}
	
	private class GerenteHandler implements ActionListener{
		public void actionPerformed(ActionEvent event) {
			if(event.getSource() == cadastrarGerente)
			{
				cadastroGerente = new CadastroGerente();
				cadastroGerente.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				cadastroGerente.setLocation(500,200);
				cadastroGerente.setSize(400,200);
				cadastroGerente.setVisible(true);
				LoginGerente.telaGerente.setVisible(false);
			}
			if(event.getSource() == cadastrarCliente)
			{
				cadastraCliente = new CadastraCliente();
				cadastraCliente.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				cadastraCliente.setLocation(500,200);
				cadastraCliente.setSize(400,200);
				cadastraCliente.setVisible(true);
				LoginGerente.telaGerente.setVisible(false);
			}
			
		}
	}

}
