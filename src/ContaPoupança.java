
public class ContaPoupanša extends Conta
{
	private double juros;
	
	public ContaPoupanša(String tipo, String correntista, int numero) {
		super(tipo, correntista,numero);
	}
	public void setJuros(double juros)
	{
		this.juros = juros;
	}
	public double getJuros()
	{
		return juros;
	}
}
