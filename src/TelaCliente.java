import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
public class TelaCliente extends JFrame {
	
	private JButton sacar;
	private JButton depositar;
	private JButton verificaSaldo;
	private JButton voltar;
	
	public TelaCliente(){
		super("Banco Digital");
		setLayout(new GridLayout(2,2,100,100));
		
		sacar = new JButton("Sacar");
		add(sacar);
		
		depositar = new JButton("DEPOSITO");
		add(depositar);
		
		verificaSaldo= new JButton("SALDO");
		add(verificaSaldo);
		
		voltar = new JButton("VOLTAR");
		add(voltar);
	}
}
