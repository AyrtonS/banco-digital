
abstract class Conta {
	
	private int numeroConta;
	private String nome;
	protected double saldo;
	private String tipo;
	
	public Conta(String tipo, String nome, int numero)
	{
		this.tipo = tipo;
		this.nome = nome;
		numeroConta = numero;
		saldo = 0;
	}
	
	public String getTipo() {
		return tipo;
	}

	public int getNumero()
	{
		return numeroConta;
	}
	public String getNome()
	{
		return nome;
	}
	public double getSaldo()
	{
		return saldo;
	}
	
	public boolean sacar(double valor)
	{
		if(saldo >= valor )
		{
			saldo = saldo-valor;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void depositar(double valor)
	{
		saldo = saldo + valor;
	}
	
}
