import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CadastroGerente extends JFrame{
	private JLabel nome;
	private JLabel senha;
	private JTextField nomeG;
	private JPasswordField senhaG;
	private JButton ok;
	private JButton cancel;
	
	public CadastroGerente() {
		super("Cadastro Gerente");
		setLayout(new GridLayout(3,2,0,20));
		nome = new JLabel("Nome:");
		nome.setHorizontalAlignment(SwingConstants.LEFT);
		add(nome);
		
		nomeG = new JTextField();
		add(nomeG);
		
		senha = new JLabel("Senha:");
		senha.setHorizontalAlignment(SwingConstants.LEFT);
		add(senha);
		
		senhaG = new JPasswordField();
		add(senhaG);
		
		ok = new JButton("Ok");
		add(ok);
		
		cancel = new JButton("Cancelar");
		add(cancel);
		
		CadastroHandler handlerLogin = new CadastroHandler();
		cancel.addActionListener(handlerLogin);
		ok.addActionListener(handlerLogin);
	}
	
	private class CadastroHandler implements ActionListener{
		public void actionPerformed(ActionEvent event) {
			if(event.getSource() == cancel)
			{
				Banco.telaPrincipal.setVisible(true);
				Banco.cadastroGerente.setVisible(false);
			}
			else if(event.getSource() == ok)
			{
				String nome = nomeG.getText();
				String senha = new String( senhaG.getPassword());
				if(Programa.banco.cadastraGerente(nome, senha))
				{
					JOptionPane.showMessageDialog(null,"Cadastro Realizado com sucesso","Cadastro",JOptionPane.INFORMATION_MESSAGE);
					Banco.telaPrincipal.setVisible(true);
					Banco.cadastroGerente.setVisible(false);
				}
				else
				{
					JOptionPane.showMessageDialog(null,"Erro no Cadastro! A Lista de Gerente Esta Completa","Cadastro",JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}
}
