import javax.swing.JFrame;

public class Banco {
	private Gerente gerentes[] = new Gerente[30];
	private String usuario;
	static TelaPrincipal telaPrincipal;
	static CadastroGerente cadastroGerente;
	public Banco()
	{
		telaPrincipal = new TelaPrincipal();
		telaPrincipal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		telaPrincipal.setLocation(400,20);
		telaPrincipal.setSize(800,800);
		if(gerentes[0]==null)
		{
			cadastroGerente = new CadastroGerente();
			cadastroGerente.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			cadastroGerente.setLocation(500,200);
			cadastroGerente.setSize(400,200);
			cadastroGerente.setVisible(true);
		}
		else
		{
			telaPrincipal.setVisible(true);
		}
	}
	public Gerente[] getGerentes() {
		return gerentes;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public Gerente getGerente(String nome) {
		for(int i=0; gerentes[i]!=null && i<30;i++)
		{
			if((gerentes[i].getNome()).equals(nome))
				return gerentes[i];
		}
		return null;
	}
	public boolean cadastraGerente(String nome, String senha)
	{
		for(int i = 0; i<30; i++)
		{
			if(gerentes[i] == null)
			{
				gerentes[i] = new Gerente(nome,senha);
				return true;
			}
		}
		return false;
	}
	public Cliente getCliente(String nome)
	{
		Cliente cliente;
		for(int i=0; gerentes[i] !=null && i<30;i++)
		{
			cliente = gerentes[i].getCliente(nome);
			if(cliente != null)
			{
				return cliente;
			}
		}
		return null;
	}
	public boolean loginCliente(String nome, String senha)
	{
		Cliente cliente;
		
		cliente = getCliente(nome);
		if(cliente != null)
		{
			if((cliente.verificaSenha(senha)))
			{
				setUsuario(nome);
				return true;
			}
		}
		return false;
	}
	public boolean loginGerente(String nome, String senha)
	{
		Gerente gerente;
		gerente = getGerente(nome);
		if(gerente != null)
		{
			if((gerente.verificaSenha(senha)))
			{
				setUsuario(nome);
				return true;
			}
		}
		return false;
	}
	public boolean cadastraCliente(String nome, String Senha)
	{
		Gerente gerente;
		gerente = getGerente(usuario);
		if(gerente.cadastraCliente(nome, Senha))
		{
			return true;
		}
		return false;
	}

}
