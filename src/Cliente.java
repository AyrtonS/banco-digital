
public class Cliente extends Pessoa {
	private Conta contas[] = new Conta[30];
	
	public Cliente(String nome, String senha) {
		super(nome, senha);
	}
	
	public Conta[] getContas() {
		return contas;
	}
	
	public int abrirConta(String tipo, String nome) {
		int i;
		for(i = 0; i < 30; i++)
		{
			if(contas[i]==null) //busca no array de contas uma conta vazia
			{
				if(tipo.equals("Simples")) //verifica o tipo da conta
				{
					contas[i] = new ContaSimples(tipo,nome,i);
					return i; //retorna numero da conta
				}
				else if(tipo.equals("Especial"))
				{
					contas[i] = new ContaEspecial(tipo,nome,i);
					return i; //retorna numero da conta
				}
				else if(tipo.equals("Poupan�a"))
				{
					contas[i] = new ContaPoupan�a(tipo,nome,i);
					return i; //retorna numero da conta
				}
			}
		}
		return -3; //retorna erro caso ja tenha alcan�ado limite de contas
	}
	
	public boolean verificaConta(int numero)
	{
		if(contas[numero] != null)//como o numero da conta � a posi��o dela no vetor, verifica se ela existe
		{
			return true;
		}
		return false;
	}
	
	public boolean deposito(int numero, double valor) {
		
		if( verificaConta(numero))
		{
			contas[numero].depositar(valor);// se existir, efetua o deposito
			return true;
		}
		return false;// conta nao existe
	}
	
	public int saque(int numero, double valor) {
		
		if(valor<= 110000)
		{
			if(verificaConta(numero))
			{
				if(contas[numero].sacar(valor))
				{
					return 1;// retorna exito
				}
				else
				{
					return 2;//codigo de saldo insuficiente
				}
			}
			else
			{
				return -1;// conta inexistente
			}
		}
		return 3; //valor maior que 110000
	}
	
	public double getSaldo(int numero) {
		
		return contas[numero].getSaldo();
	}
}
