import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CadastraCliente extends JFrame{
	private JLabel nome;
	private JLabel senha;
	private JTextField nomeC;
	private JPasswordField senhaC;
	private JButton ok;
	private JButton cancel;
	
	public CadastraCliente() {
		super("Cadastro Cliente");
		setLayout(new GridLayout(3,2,0,20));
		nome = new JLabel("Nome:");
		nome.setHorizontalAlignment(SwingConstants.LEFT);
		add(nome);
		
		nomeC = new JTextField();
		add(nomeC);
		
		senha = new JLabel("Senha:");
		senha.setHorizontalAlignment(SwingConstants.LEFT);
		add(senha);
		
		senhaC = new JPasswordField();
		add(senhaC);
		
		ok = new JButton("Ok");
		add(ok);
		
		cancel = new JButton("Cancelar");
		add(cancel);
		
		CadastroHandler handlerCadastro = new CadastroHandler();
		cancel.addActionListener(handlerCadastro);
		ok.addActionListener(handlerCadastro);
	}
	private class CadastroHandler implements ActionListener{
		public void actionPerformed(ActionEvent event) {
			if(event.getSource() == cancel)
			{
				Banco.telaPrincipal.setVisible(true);
				Banco.cadastroGerente.setVisible(false);
			}
			else if(event.getSource() == ok)
			{
				String nome = nomeC.getText();
				String senha = new String( senhaC.getPassword());
				if(Programa.banco.cadastraCliente(nome, senha))
				{
					JOptionPane.showMessageDialog(null,"Cadastro Realizado com sucesso","Cadastro",JOptionPane.INFORMATION_MESSAGE);
					LoginGerente.telaGerente.setVisible(true);
					TelaGerente.cadastraCliente.setVisible(false);
				}
				else
				{
					JOptionPane.showMessageDialog(null,"Erro no Cadastro! A Lista de Gerente Esta Completa","Cadastro",JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}
}
